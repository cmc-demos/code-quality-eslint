// Sample JavaScript file for ESLint testing
//newline

// Variable declaration
let greeting = 'Hello, World!';

// Function declaration
function greet(name) {
    return `Hello, ${name}!`;
}

// Arrow function
const add = (a, b) => {
    return a + b;
};

// Conditional statement
if (true) {
    console.log(greeting);
} else {
    console.log('Goodbye!');
}

// Loop
for (let i = 0; i < 5; i++) {
    console.log(i);
}

// Array
const numbers = [1, 2, 3, 4, 5];

// Iterating through array
numbers.forEach(num => {
    console.log(num);
});

// Object
const person = {
    name: 'John',
    age: 30,
    profession: 'Developer'
};

// Destructuring
const { name, age } = person;
console.log(`Name: ${name}, Age: ${age}`);

// Exporting module
export { greet, add };
